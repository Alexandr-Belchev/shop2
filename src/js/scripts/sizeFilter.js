// size filter

let cards = document.getElementsByClassName('cards__item');
let inputsSize = document.getElementsByClassName('form__size');
let filterBtnsSize = Array.from(inputsSize);
let arrSize = [];
filterBtnsSize.forEach(function (el) {
    el.onclick = function () {
        console.log(el.value);
        let name = el.name;
        let index = arrSize.indexOf(name);
        if (index === -1) {
            arrSize.push(name)
        } else {
            arrSize.splice(name, 1)
        }
        filterSize(arrSize, cards)
    }
});

function filterSize(filtersArraySize, elements) {
    let elArraySize = Array.from(elements);
    if (filtersArraySize.length) {
        let newArrSize = elArraySize.filter(function (el) {
            console.log(el);
            return filtersArraySize.some(filter => {
                console.log(filter, el.querySelector('.feature__size').dataset.sizes);
                return el.querySelector('.feature__size').dataset.sizes == filter;
            })
        });
        console.log(newArrSize);

        elArraySize.forEach(function (el) {
            el.classList.add('is-passive')
        });
        newArrSize.forEach(function (el) {
            el.classList.remove('is-passive')
        })

    } else {
        elArraySize.forEach(function (el) {
            el.classList.remove('is-passive')
        })
    }
}
