// add hidden class for filter box
let filter__box = document.getElementsByClassName('filter__box');
let filterBox = Array.from(filter__box);
filterBox.forEach(function (el) {
    if (window.innerWidth <= 720) {
        el.classList.add('hidden')
        el.classList.add('transition')
    }
    else {
        el.classList.remove('hidden')
        el.classList.remove('transition')

    }
})

// location reload ( product page )

window.onresize = function () {
    window.location.reload();
};