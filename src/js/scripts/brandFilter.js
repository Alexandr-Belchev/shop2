// brand filter
let cards = document.getElementsByClassName('cards__item');
let inputs = document.getElementsByClassName('form__brand');
let filterBtns = Array.from(inputs);

let arr = [];
filterBtns.forEach(function (el) {
    el.onclick = function () {
        console.log(el.name);
        let name = el.name;
        let index = arr.indexOf(name);
        if (index === -1) {
            arr.push(name)
        } else {
            arr.splice(index, 1)
        }
        filter(arr, cards)
    }
});

function filter(filtersArray, elements) {
    let elArray = Array.from(elements);
    if (filtersArray.length) {
        let newArr = elArray.filter(function (el) {
            console.log(el);
            return filtersArray.some(filter => {
                console.log(filter, el.querySelector('.cards__title').dataset.brand);
                return el.querySelector('.cards__title').dataset.brand == filter;
            })
        });
        console.log(newArr);

        elArray.forEach(function (el) {
            el.classList.add('is-passive')
        });
        newArr.forEach(function (el) {
            el.classList.remove('is-passive')
        })

    } else {
        elArray.forEach(function (el) {
            el.classList.remove('is-passive')
        })
    }
}