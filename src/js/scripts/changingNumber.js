// chanche number in cart (view.html)
window.onload=function () {
    let quantity = 1;
    let quantityEl = document.querySelector('.quantity');
    let _plus = document.querySelector('.counter__plus') ;
    let _minus = document.querySelector('.counter__minus');
    let view__price = document.querySelector('.view__price');
    let totalPrice = parseInt(view__price.innerHTML)
    if(_plus) {
        _plus.onclick = function () {
            quantity++;
            quantityEl.value = quantity;
            totalPrice=quantity*view__price.dataset.value
            view__price.innerHTML=totalPrice + '$'
            console.log(totalPrice)
        };
    }
    if(_minus) {
        _minus.onclick = function () {
            if (quantity > 1) {
                quantity--;
                quantityEl.value = quantity;
                totalPrice=totalPrice-view__price.dataset.value;
                view__price.innerHTML=totalPrice + '$'
                console.log(totalPrice)
            }
        }
    }
};