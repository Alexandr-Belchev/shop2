// menu
let mobile = document.querySelector('.mobile')
let sidebar = document.querySelector('.sidebar')
let overlay = document.querySelector('.overlay')
let sidebar__close = document.querySelector('.sidebar__close')

mobile.onclick=function () {
    sidebar.classList.add('menu--active')
    overlay.classList.add('active')
}
sidebar__close.onclick=function () {
    sidebar.classList.remove('menu--active')
    overlay.classList.remove('active')
}