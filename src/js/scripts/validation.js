//validation
$.validator.addMethod("emailMethod", function(value, element) {
    let pattern = /^\w+@[a-zA-Z_0-9]+?\.[a-zA-Z]{2,3}$/;
    return this.optional(element) || pattern.test(value);
});
$('.form__validate').validate({
    rules: {
        email: {
            required: true,
            emailMethod: true
        }
    },
    messages: {
        email: {
            email: "Please enter a valid email address. (user@gmail.com)"
        }
    }
});