// flying image (view.html)
let quantity = document.querySelector('.cart__quantity');
let order1 = document.querySelector('.order');
let button__cart = document.querySelector('.button__cart');
let element = document.querySelector('.slider__image');
let _number;
if(order1) {
    order1.onclick = function () {
        if (window.innerWidth >= 960) {
            let c = button__cart.getBoundingClientRect(),
                scrolltop = document.body.scrollTop + c.top,
                scrollleft = document.body.scrollLeft + c.left;
            _number = quantity.dataset.value++
            quantity.innerHTML = _number+1
            let clone = element.cloneNode();
            console.log(clone);
            clone.style.position = 'absolute';
            clone.style.left = '220px';
            clone.style.top = '300px';
            clone.style.animation = 'viewImage 2.5s forwards';
            clone.style.transition = '2s linear all';
            clone.style.zIndex = '99'
            let body = document.querySelector('body');
            body.append(clone);
            setTimeout(() => {
                clone.style.left = scrollleft + 'px'
                clone.style.top = scrolltop + 'px'

            }, 100)
        }
        ;
    }
}