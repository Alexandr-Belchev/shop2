
//show more
let moreB = document.querySelector('.more-button');
moreB.onclick = function showMore() {

    let listData = Array.prototype.slice.call(document.querySelectorAll('.cards .cards__item:not(.shown)')).slice(0, 1);

    for (let i=0; i < listData.length; i++)
    {
        listData[i].className  = 'cards__item shown';
    }
};
