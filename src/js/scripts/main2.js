

// location reload ( product page )
/*if (window.innerWidth <= 720) {
    if (! localStorage.justOnce) {
        localStorage.setItem("justOnce", "true");
        window.location.reload();
    }
}*/
// dropdown
// dropdown
$(function () {
    $('.accordion')
        .accordion({
            selector: {
                trigger: '.title'
            }
        })
    ;
});











// size filter

    let inputsSize = document.getElementsByClassName('form__size');
    let filterBtnsSize = Array.from(inputsSize);
    let arrSize = [];
    filterBtnsSize.forEach(function (el) {
        el.onclick = function () {
            console.log(el.value);
            let name = el.name;
            let index = arrSize.indexOf(name);
            if (index === -1) {
                arrSize.push(name)
            } else {
                arrSize.splice(name, 1)
            }
            filterSize(arrSize, cards)
        }
    });

    function filterSize(filtersArraySize, elements) {
        let elArraySize = Array.from(elements);
        if (filtersArraySize.length) {
            let newArrSize = elArraySize.filter(function (el) {
                console.log(el);
                return filtersArraySize.some(filter => {
                    console.log(filter, el.querySelector('.feature__size').dataset.sizes);
                    return el.querySelector('.feature__size').dataset.sizes == filter;
                })
            });
            console.log(newArrSize);

            elArraySize.forEach(function (el) {
                el.classList.add('is-passive')
            });
            newArrSize.forEach(function (el) {
                el.classList.remove('is-passive')
            })

        } else {
            elArraySize.forEach(function (el) {
                el.classList.remove('is-passive')
            })
        }
    }


//category filter

    let inputsCategory = document.getElementsByClassName('form__category');
    let filterBtnsCategory = Array.from(inputsCategory);
    let arrCategory = [];
    filterBtnsCategory.forEach(function (el) {
        el.onclick = function () {
            console.log(el.value);
            let name = el.name;
            let index = arrCategory.indexOf(name);
            if (index === -1) {
                arrCategory.push(name)
            } else {
                arrCategory.splice(name, 1)
            }
            filterCategory(arrCategory, cards)
        }
    });

    function filterCategory(filtersArrayCategory, elements) {
        let elArrayCategory = Array.from(elements);
        if (filtersArrayCategory.length) {
            let newArrCategory = elArrayCategory.filter(function (el) {
                console.log(el);
                return filtersArrayCategory.some(filter => {
                    console.log(filter, el.querySelector('.cards__category').dataset.category);
                    return el.querySelector('.cards__category').dataset.category == filter;
                })
            });
            console.log(newArrCategory);

            elArrayCategory.forEach(function (el) {
                el.classList.add('is-passive')
            });
            newArrCategory.forEach(function (el) {
                el.classList.remove('is-passive')
            })

        } else {
            elArrayCategory.forEach(function (el) {
                el.classList.remove('is-passive')
            })
        }
    }

// price filter

$(function () {
    let $range = $(".slider-range"),
        $from = $(".range__from"),
        $to = $(".range__to"),
        min = 0,
        max = 1000,
        from,
        rangeArr=[],
        to;
    $range.ionRangeSlider({
        type: "double",
        min: min,
        max: max,
        onChange: function (data) {
            from = data.from;
            to = data.to;
            rangeArr=[from, to]
            console.log(rangeArr)
            getRangeValues(rangeArr)
            filterRange(rangeArr, cards)
        }
    });
});
let range__from = document.querySelector('.range__from')
let range__to = document.querySelector('.range__to')
function getRangeValues(sd) {
    range__from.value=sd[0]+'$'
    range__to.value=sd[1]+'$'
}

let inputsRange = document.getElementsByClassName('form__category');
let filterBtnsRange = Array.from(inputsRange);
let arrRange = [];
// filterBtnsRange.forEach(function (el) {
//     el.change = function () {
//         console.log(el.value);
//         let value = el.value;
//         let index = arrRange.indexOf(value);
//         if (index === -1) {
//             arrRange.push(value)
//         } else {
//             arrRange.splice(value, 1)
//         }
//         filterRange(arrRange, cards)
//     }
// });
function filterRange(filtersArrayRange, elements) {
    let elArrayRange = Array.from(elements);
    // let arr = elArrayRange.filter( el => {
    //     return +el.querySelector('.cards__price').dataset.price >= filtersArrayRange[0] && +el.querySelector('.cards__price').dataset.price <= filtersArrayRange[1]
    // })
    if (filtersArrayRange.length) {
        let newArrRange = elArrayRange.filter(function (el) {
            console.log(el);
            return +el.querySelector('.cards__price').dataset.price >= filtersArrayRange[0] &&
                el.querySelector('.cards__price').dataset.price <= filtersArrayRange[1]

        });
        console.log(newArrRange);

        elArrayRange.forEach(function (el) {
            el.classList.add('is-passive')
        });
        newArrRange.forEach(function (el) {
            el.classList.remove('is-passive')
        })

    } else {
        elArrayRange.forEach(function (el) {
            el.classList.remove('is-passive')
        })
    }
}

//show more
let moreB = document.querySelector('.more-button');
moreB.onclick = function showMore() {

    let listData = Array.prototype.slice.call(document.querySelectorAll('.cards .cards__item:not(.shown)')).slice(0, 1);

    for (let i=0; i < listData.length; i++)
    {
        listData[i].className  = 'cards__item shown';
    }
};




