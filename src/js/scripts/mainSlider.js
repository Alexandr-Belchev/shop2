// main slider
$(document).ready(function() {
    $('.offers__slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        arrows: false,
        autoplaySpeed: 3000,
        dots: true
    });
    $(".lazy").slick({
        lazyLoad: 'ondemand',
        infinite: true
    });

});