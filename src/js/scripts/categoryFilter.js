//category filter

let cards = document.getElementsByClassName('cards__item');
let inputsCategory = document.getElementsByClassName('form__category');
let filterBtnsCategory = Array.from(inputsCategory);
let arrCategory = [];
filterBtnsCategory.forEach(function (el) {
    el.onclick = function () {
        console.log(el.value);
        let name = el.name;
        let index = arrCategory.indexOf(name);
        if (index === -1) {
            arrCategory.push(name)
        } else {
            arrCategory.splice(name, 1)
        }
        filterCategory(arrCategory, cards)
    }
});

function filterCategory(filtersArrayCategory, elements) {
    let elArrayCategory = Array.from(elements);
    if (filtersArrayCategory.length) {
        let newArrCategory = elArrayCategory.filter(function (el) {
            console.log(el);
            return filtersArrayCategory.some(filter => {
                console.log(filter, el.querySelector('.cards__category').dataset.category);
                return el.querySelector('.cards__category').dataset.category == filter;
            })
        });
        console.log(newArrCategory);

        elArrayCategory.forEach(function (el) {
            el.classList.add('is-passive')
        });
        newArrCategory.forEach(function (el) {
            el.classList.remove('is-passive')
        })

    } else {
        elArrayCategory.forEach(function (el) {
            el.classList.remove('is-passive')
        })
    }
}