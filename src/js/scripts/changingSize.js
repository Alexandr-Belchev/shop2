// add size to title by click (view.html)
let size__button = document.getElementsByClassName('size__button');
let filterBtnsSize2 = Array.from(size__button);
filterBtnsSize2.forEach(function (el) {
    let viewTitle = document.querySelector('.offers__title--view')
    if(el.classList.contains('active__size')){
        let titleAdd = ' ' + el.dataset.viewsize
        viewTitle.innerHTML += titleAdd
    }
    el.onclick=function () {
        let clearTitle = viewTitle.dataset.title
        viewTitle.innerHTML = clearTitle
        filterBtnsSize2.forEach(function (el) {
            // if(el.classList.contains('active__size')) {
            el.classList.remove('active__size')
            // }
        })
        if(el.classList.contains('active__size')){
            el.classList.remove('active__size')
        }else {
            el.classList.add('active__size')
            console.log(el.dataset.viewsize)
            let titleAdd = ' ' + el.dataset.viewsize
            viewTitle.innerHTML += titleAdd
        }
    }
})