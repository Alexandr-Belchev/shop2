// price filter

$(function () {
    let $range = $(".slider-range"),
        $from = $(".range__from"),
        $to = $(".range__to"),
        min = 0,
        max = 1000,
        from,
        rangeArr=[],
        to;
    $range.ionRangeSlider({
        type: "double",
        min: min,
        max: max,
        onChange: function (data) {
            from = data.from;
            to = data.to;
            rangeArr=[from, to]
            console.log(rangeArr)
            getRangeValues(rangeArr)
            filterRange(rangeArr, cards)
        }
    });
});
let cards = document.getElementsByClassName('cards__item');
let range__from = document.querySelector('.range__from')
let range__to = document.querySelector('.range__to')
function getRangeValues(sd) {
    range__from.value=sd[0]+'$'
    range__to.value=sd[1]+'$'
}

let inputsRange = document.getElementsByClassName('form__category');
let filterBtnsRange = Array.from(inputsRange);
let arrRange = [];
function filterRange(filtersArrayRange, elements) {
    let elArrayRange = Array.from(elements);
    if (filtersArrayRange.length) {
        let newArrRange = elArrayRange.filter(function (el) {
            console.log(el);
            return +el.querySelector('.cards__price').dataset.price >= filtersArrayRange[0] &&
                el.querySelector('.cards__price').dataset.price <= filtersArrayRange[1]

        });
        console.log(newArrRange);

        elArrayRange.forEach(function (el) {
            el.classList.add('is-passive')
        });
        newArrRange.forEach(function (el) {
            el.classList.remove('is-passive')
        })

    } else {
        elArrayRange.forEach(function (el) {
            el.classList.remove('is-passive')
        })
    }
}
