//import 'vue'
//
import $ from 'jquery'
import slider from 'slick-carousel/slick/slick.min.js'
window.$ = window.jQuery = $
window.slider = slider


import jQueryValidation from 'jquery-validation'
window.jQueryValidation = jQueryValidation

import ionRangeSlider from 'ion-rangeslider'
window.ionRangeSlider = ionRangeSlider

